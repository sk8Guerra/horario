var Grupo = function() {
  var main = this;
  var uri = '/api/grupos';

  main.grupos = ko.observableArray([]);

  main.getGrupos = function() {
    ajaxHelper(uri, 'GET').done(function(data) {
      main.grupos(data);
    });
  }

  main.gn = {
    nombre: ko.observable()
  }

  main.agregar = function () {
    var grupo = {
      nombre: main.gn.nombre()
    }
    ajaxHelper(uri, 'POST', grupo).done(function(data) {
      $('#addModal').modal('hide');
      limpiar();
      main.getGrupos();
    });
  }

  function ajaxHelper(uri, method, data) {
    return $.ajax({
      url: uri,
      type: method,
      dataType: 'json',
      contentType: 'application/json',
      data: data ? JSON.stringify(data) : null
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.log(errorThrown);
    });
  }

  function limpiar() {
    main.gn.nombre(null);
  }

  main.getGrupos();
}

$(document).ready(function() {
  var grupo = new Grupo();
  ko.applyBindings(grupo);
})
