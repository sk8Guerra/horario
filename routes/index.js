var express = require('express');
var router = express.Router();
var grupo = require('../model/grupo');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

router.get('/grupos', function(req, res, next) {
    res.render('grupos', { title: 'Grupos' });
});

router.get('/api/grupos', function(req, res, next) {
  grupo.selectAll(function(error, resultados){
    if(typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay grupos"});
    }
  });
});

router.post('/api/grupos', function(req, res) {
  var data = {
    nombre: req.body.nombre
  }
  grupo.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
      res.json(resultado);
    } else {
      res.json({"Mensaje": "No se ingreso la categoria"});
    }
  });
});

router.get('/cursos', function(req, res, next) {
  res.render('cursos', { title: 'Cursos' });
});

module.exports = router;
