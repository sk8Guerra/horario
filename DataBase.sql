CREATE DATABASE Horario;
USE Horario;

CREATE TABLE Grupo(
	idGrupo INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (50) NOT NULL
);

CREATE TABLE Curso(
	idCurso INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (50) NOT NULL
);

CREATE TABLE CursoGrupo(
	idCursoGrupo INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idGrupo INT NOT NULL,
    idCurso INT NOT NULL,
    FOREIGN KEY (idGrupo) REFERENCES Grupo(idGrupo) ON DELETE CASCADE,
    FOREIGN KEY (idCurso) REFERENCES Curso(idCurso) ON DELETE CASCADE
);

CREATE VIEW Grupos AS
	SELECT idGrupo AS 'ID', nombre AS 'Nombre' FROM Grupo;

 DELIMITER $$

CREATE PROCEDURE VerificarOInsertarGrupo(IN n VARCHAR(50))
	BEGIN
		IF EXISTS (SELECT * FROM Grupo WHERE nombre = n) THEN
			SELECT idGrupo, nombre FROM Grupo WHERE nombre = n;
		ELSE 
			INSERT INTO Grupo (nombre) VALUES (n);
		END IF;
	END $$
    
 DELIMITER $$
 
CREATE PROCEDURE VerificarOInsertarCurso(IN n VARCHAR(50))
	BEGIN
		IF EXISTS (SELECT * FROM Curso WHERE nombre = n) THEN
			SELECT idCurso, nombre FROM Curso WHERE nombre = n;
		ELSE 
			INSERT INTO Curso (nombre) VALUES (n);
		END IF;
	END $$
    
 DELIMITER $$
 
CREATE PROCEDURE VerificarOInsertarCursoGrupo(IN g INT, IN c INT)
	BEGIN
		IF EXISTS (SELECT * FROM CursoGrupo WHERE idGrupo = g AND idCurso = c) THEN
			SELECT idCursoGrupo, idGrupo, idCurso FROM CursoGrupo WHERE idGrupo = g AND idCurso = c;
		ELSE 
			INSERT INTO CursoGrupo (idGrupo, idCurso) VALUES (g, c);
		END IF;
	END $$