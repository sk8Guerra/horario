var database = require('./database');
var grupo = {};

grupo.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM Grupos",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}

grupo.insert = function(data, callback) {
  if(database) {
    database.query("CALL VerificarOInsertarGrupo(?)", data.nombre,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = grupo;
